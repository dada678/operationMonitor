package com.htdf.controller;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {
	
	@RequestMapping("/")
    public String velocityTest(Map map){
        map.put("message", "这是测试的内容。。。");
        map.put("toUserName", "张三1");
        map.put("fromUserName", "老许");
        return "index";
    }

}
